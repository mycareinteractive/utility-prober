﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Prober.Lg.Api
{
    public class RmsParameter : IRmsXml
    {
        public string Name { get; set; }

        public IRmsXml Value { get; set; }

        public RmsParameter()
        {
            Name = "";
            Value = null;
        }

        public RmsParameter(string name)
        {
            Name = name;
            Value = null;
        }

        public RmsParameter(string name, IRmsXml value)
        {
            Name = name;
            Value = value;
        }

        public void ToXml(XmlWriter writer)
        {
            writer.WriteStartElement("name");
            writer.WriteString(Name);
            writer.WriteEndElement();
            writer.WriteStartElement("value");
            Value.ToXml(writer);
            writer.WriteEndElement();
        }

        public void FromXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
