﻿using Prober.Common;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Prober.Lg.Api
{
    public class LgClient
    {
        private readonly int port = 9001;

        private ILogger logger;

        public LgClient(ILogger log)
        {
            logger = log;
        }

        public bool LogInitialize(string ip)
        {
            var request = new RmsRequest(RmsMethods.ControlLog, RmsActions.LogInitialize, RmsResults.Http);
            var param = new RmsComplexType();
            param.List.Add("ip", ip);
            param.List.Add("port", "23");
            param.List.Add("direction_path", "telnet");
            request.Parameters.Add(new RmsParameter("parameter", param));
            return Send(request, ip);
        }

        public bool LogFinalize(string ip)
        {
            return true;
            var request = new RmsRequest(RmsMethods.ControlLog, RmsActions.LogFinalize, RmsResults.Http);
            return Send(request, ip);
        }

        public bool Reboot(string ip)
        {
            var request = new RmsRequest(RmsMethods.ControlTv, RmsActions.SystemAction, RmsResults.Http);
            var param = new RmsComplexType();
            param.List.Add("action_mode", "reboot");
            request.Parameters.Add(new RmsParameter("parameter", param));
            return Send(request, ip);
        }

        private bool Send(RmsRequest request, string dest)
        {
            bool ret = false;
            using (var client = new TcpClient())
            {
                try
                {
                    client.SendTimeout = 5000;
                    client.Connect(dest, port);

                    var message = request.ToString();
                    logger.Log(">>>>>>>>>> Request >>>>>>>>>>" + Environment.NewLine + message);
                    
                    byte[] data = Encoding.UTF8.GetBytes(message);
                    NetworkStream stream = client.GetStream();

                    stream.Write(data, 0, data.Length);
                    stream.Flush();

                    byte[] bytes = new byte[client.ReceiveBufferSize];
                    stream.Read(bytes, 0, (int)client.ReceiveBufferSize);
                    string returnMessage = Encoding.UTF8.GetString(bytes).TrimEnd('\0');

                    logger.Log("<<<<<<<<<< Response <<<<<<<<<<" + Environment.NewLine + returnMessage);
                    if (!string.IsNullOrEmpty(returnMessage) && returnMessage.Contains("SUCCESS"))
                    {
                        ret = true;
                    }
                }
                catch (Exception ex)
                {
                    logger.Log(ex.Message);
                    ret = false;
                }
            }
            return ret;
        }
    }
}
