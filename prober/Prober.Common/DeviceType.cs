﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Common
{
    public enum DeviceType
    {
        None = -1,
        Unknown = 0,
        Enseo,
        Nebula,
        Procentric
    }
}
