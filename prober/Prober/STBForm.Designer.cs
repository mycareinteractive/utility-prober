﻿namespace Prober
{
    partial class STBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(STBForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroButtonSync = new System.Windows.Forms.Button();
            this.metroTextBoxIP = new System.Windows.Forms.TextBox();
            this.metroLabel1 = new System.Windows.Forms.Label();
            this.dataGridViewSTBs = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.metroButtonFilter = new System.Windows.Forms.Button();
            this.metroTextBoxDevice = new System.Windows.Forms.TextBox();
            this.metroTextBoxRoom = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSTBs)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.metroButtonSync);
            this.groupBox1.Controls.Add(this.metroTextBoxIP);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(483, 48);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // metroButtonSync
            // 
            this.metroButtonSync.Location = new System.Drawing.Point(346, 13);
            this.metroButtonSync.Name = "metroButtonSync";
            this.metroButtonSync.Size = new System.Drawing.Size(131, 23);
            this.metroButtonSync.TabIndex = 8;
            this.metroButtonSync.Text = "Load From Database";
            this.metroButtonSync.Click += new System.EventHandler(this.metroButtonSync_Click);
            // 
            // metroTextBoxIP
            // 
            this.metroTextBoxIP.Location = new System.Drawing.Point(196, 15);
            this.metroTextBoxIP.Name = "metroTextBoxIP";
            this.metroTextBoxIP.Size = new System.Drawing.Size(130, 20);
            this.metroTextBoxIP.TabIndex = 0;
            this.metroTextBoxIP.Text = "127.0.0.1";
            this.metroTextBoxIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(6, 18);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(176, 13);
            this.metroLabel1.TabIndex = 6;
            this.metroLabel1.Text = "SeaChange Multiverse Database IP";
            // 
            // dataGridViewSTBs
            // 
            this.dataGridViewSTBs.AllowUserToAddRows = false;
            this.dataGridViewSTBs.AllowUserToDeleteRows = false;
            this.dataGridViewSTBs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSTBs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSTBs.GridColor = System.Drawing.Color.Gainsboro;
            this.dataGridViewSTBs.Location = new System.Drawing.Point(0, 99);
            this.dataGridViewSTBs.MultiSelect = false;
            this.dataGridViewSTBs.Name = "dataGridViewSTBs";
            this.dataGridViewSTBs.ReadOnly = true;
            this.dataGridViewSTBs.RowHeadersVisible = false;
            this.dataGridViewSTBs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSTBs.Size = new System.Drawing.Size(483, 351);
            this.dataGridViewSTBs.TabIndex = 4;
            this.dataGridViewSTBs.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSTBs_CellDoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.metroButtonFilter);
            this.groupBox2.Controls.Add(this.metroTextBoxDevice);
            this.groupBox2.Controls.Add(this.metroTextBoxRoom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(483, 51);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(150, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "or Device ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Room";
            // 
            // metroButtonFilter
            // 
            this.metroButtonFilter.Location = new System.Drawing.Point(346, 17);
            this.metroButtonFilter.Name = "metroButtonFilter";
            this.metroButtonFilter.Size = new System.Drawing.Size(131, 23);
            this.metroButtonFilter.TabIndex = 6;
            this.metroButtonFilter.Text = "Apply Filter";
            this.metroButtonFilter.Click += new System.EventHandler(this.metroButtonFilter_Click);
            // 
            // metroTextBoxDevice
            // 
            this.metroTextBoxDevice.Location = new System.Drawing.Point(223, 19);
            this.metroTextBoxDevice.Name = "metroTextBoxDevice";
            this.metroTextBoxDevice.Size = new System.Drawing.Size(103, 20);
            this.metroTextBoxDevice.TabIndex = 7;
            this.metroTextBoxDevice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroTextBoxRoom
            // 
            this.metroTextBoxRoom.Location = new System.Drawing.Point(47, 19);
            this.metroTextBoxRoom.Name = "metroTextBoxRoom";
            this.metroTextBoxRoom.Size = new System.Drawing.Size(97, 20);
            this.metroTextBoxRoom.TabIndex = 6;
            this.metroTextBoxRoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(483, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Blue;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(169, 17);
            this.toolStripStatusLabel1.Text = "Double Click a Device to Select";
            // 
            // STBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridViewSTBs);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(432, 300);
            this.Name = "STBForm";
            this.Text = "Browse Devices";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSTBs)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewSTBs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button metroButtonSync;
        private System.Windows.Forms.TextBox metroTextBoxIP;
        private System.Windows.Forms.Label metroLabel1;
        private System.Windows.Forms.TextBox metroTextBoxDevice;
        private System.Windows.Forms.TextBox metroTextBoxRoom;
        private System.Windows.Forms.Button metroButtonFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}