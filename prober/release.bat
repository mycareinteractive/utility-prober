rd /S /Q .\Releases\Build
mkdir ..\Releases\Build
copy /Y Prober\bin\Release\*.dll ..\Releases\Build\
copy /Y Prober\bin\Release\*.exe ..\Releases\Build\
copy /Y Prober\bin\Release\*.xml ..\Releases\Build\
del /Q /F ..\Releases\Build\*.vshost.exe