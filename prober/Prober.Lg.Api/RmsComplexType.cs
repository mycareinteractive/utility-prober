﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Lg.Api
{
    public class RmsComplexType : IRmsXml
    {
        public string Type = "list";

        public Dictionary<string, string> List { get; private set; }

        public RmsComplexType()
        {
            List = new Dictionary<string, string>();
        }

        public void ToXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("complexType");
            writer.WriteStartElement(this.Type);
            foreach(var item in List)
            {
                writer.WriteStartElement("item");
                writer.WriteAttributeString("name", item.Key);
                writer.WriteString(item.Value);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        public void FromXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
