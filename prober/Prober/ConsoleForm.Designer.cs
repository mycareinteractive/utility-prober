﻿namespace Prober
{
    partial class ConsoleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsoleForm));
            this.richTextBoxOutput = new System.Windows.Forms.RichTextBox();
            this.scrollCheckBox = new System.Windows.Forms.CheckBox();
            this.copyButton = new System.Windows.Forms.Button();
            this.openLogButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBoxOutput
            // 
            this.richTextBoxOutput.BackColor = System.Drawing.Color.Black;
            this.richTextBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxOutput.ForeColor = System.Drawing.Color.White;
            this.richTextBoxOutput.Location = new System.Drawing.Point(5, 30);
            this.richTextBoxOutput.Name = "richTextBoxOutput";
            this.richTextBoxOutput.ReadOnly = true;
            this.richTextBoxOutput.Size = new System.Drawing.Size(774, 426);
            this.richTextBoxOutput.TabIndex = 0;
            this.richTextBoxOutput.Text = "Starting console...\n";
            this.richTextBoxOutput.WordWrap = false;
            this.richTextBoxOutput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBoxOutput_KeyPress);
            // 
            // scrollCheckBox
            // 
            this.scrollCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.scrollCheckBox.AutoSize = true;
            this.scrollCheckBox.Checked = true;
            this.scrollCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scrollCheckBox.Location = new System.Drawing.Point(198, 1);
            this.scrollCheckBox.Name = "scrollCheckBox";
            this.scrollCheckBox.Size = new System.Drawing.Size(62, 23);
            this.scrollCheckBox.TabIndex = 2;
            this.scrollCheckBox.Text = "Scroll ON";
            this.scrollCheckBox.UseVisualStyleBackColor = true;
            this.scrollCheckBox.CheckedChanged += new System.EventHandler(this.scrollCheckBox_CheckedChanged);
            // 
            // copyButton
            // 
            this.copyButton.Image = global::Prober.Properties.Resources.copy2;
            this.copyButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.Location = new System.Drawing.Point(8, 1);
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(75, 23);
            this.copyButton.TabIndex = 3;
            this.copyButton.Text = "Copy All";
            this.copyButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // openLogButton
            // 
            this.openLogButton.AutoSize = true;
            this.openLogButton.Location = new System.Drawing.Point(89, 1);
            this.openLogButton.Name = "openLogButton";
            this.openLogButton.Size = new System.Drawing.Size(103, 23);
            this.openLogButton.TabIndex = 4;
            this.openLogButton.Text = "Open Console File";
            this.openLogButton.UseVisualStyleBackColor = true;
            this.openLogButton.Click += new System.EventHandler(this.openLogButton_Click);
            // 
            // ConsoleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.openLogButton);
            this.Controls.Add(this.copyButton);
            this.Controls.Add(this.scrollCheckBox);
            this.Controls.Add(this.richTextBoxOutput);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(250, 250);
            this.Name = "ConsoleForm";
            this.Padding = new System.Windows.Forms.Padding(5, 30, 5, 5);
            this.Text = "Console";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConsoleForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxOutput;
        private System.Windows.Forms.CheckBox scrollCheckBox;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button openLogButton;
    }
}