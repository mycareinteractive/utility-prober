﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Prober.Lg.Api
{
    public class RmsRequest
    {
        public static int Id {get; private set;}

        public string Method { get; set; }

        public List<RmsParameter> Parameters { get; set; }

        public RmsRequest(string method, string action, string result)
        {
            Id++;
            Method = method;
            Parameters = new List<RmsParameter>();
            if (action != null)
            {
                var actionValue = new RmsSimpleType(action);
                var actionParameter = new RmsParameter("request", actionValue);
                Parameters.Add(actionParameter);
            }
            if (result != null)
            {
                var resultValue = new RmsSimpleType(result);
                var resultParameter = new RmsParameter("send_result", resultValue);
                Parameters.Add(resultParameter);
            }
        }

        public override string ToString()
        {
            MemoryStream memoryStream = new MemoryStream();
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Encoding = new UTF8Encoding(false);
            xmlWriterSettings.ConformanceLevel = ConformanceLevel.Document;
            xmlWriterSettings.Indent = true;

            using (XmlWriter writer = XmlWriter.Create(memoryStream, xmlWriterSettings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("rms");
                writer.WriteStartElement("request");

                writer.WriteStartElement("requestid");
                writer.WriteString(Id.ToString());
                writer.WriteEndElement(); // requestid

                writer.WriteStartElement("method");
                writer.WriteString(Method);
                writer.WriteEndElement(); // method

                writer.WriteStartElement("parameters");
                foreach (var param in Parameters)
                {
                    writer.WriteStartElement("parameter");
                    param.ToXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement(); // parameters

                writer.WriteEndElement(); // request
                writer.WriteEndElement(); // rms
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }

            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

    }
}
