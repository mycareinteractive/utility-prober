﻿namespace Prober
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.metroPanelOps = new System.Windows.Forms.Panel();
            this.metroButtonContentUpdate = new System.Windows.Forms.Button();
            this.metroButtonFirmwarePull = new System.Windows.Forms.Button();
            this.metroButtonReboot = new System.Windows.Forms.Button();
            this.metroButtonConsole = new System.Windows.Forms.Button();
            this.metroButtonReload = new System.Windows.Forms.Button();
            this.metroToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.imageButton0 = new System.Windows.Forms.ImageButton();
            this.metroTextBoxIP = new System.Windows.Forms.TextBox();
            this.imageButton9 = new System.Windows.Forms.ImageButton();
            this.imageButton8 = new System.Windows.Forms.ImageButton();
            this.imageButton7 = new System.Windows.Forms.ImageButton();
            this.imageButton6 = new System.Windows.Forms.ImageButton();
            this.imageButton5 = new System.Windows.Forms.ImageButton();
            this.imageButton4 = new System.Windows.Forms.ImageButton();
            this.imageButton3 = new System.Windows.Forms.ImageButton();
            this.imageButton2 = new System.Windows.Forms.ImageButton();
            this.imageButton1 = new System.Windows.Forms.ImageButton();
            this.imageButtonMute = new System.Windows.Forms.ImageButton();
            this.imageButtonPrev = new System.Windows.Forms.ImageButton();
            this.imageButtonVolDown = new System.Windows.Forms.ImageButton();
            this.imageButtonVolUp = new System.Windows.Forms.ImageButton();
            this.imageButtonChDown = new System.Windows.Forms.ImageButton();
            this.imageButtonChUp = new System.Windows.Forms.ImageButton();
            this.imageButtonMenu = new System.Windows.Forms.ImageButton();
            this.imageButtonCC = new System.Windows.Forms.ImageButton();
            this.imageButtonPlay = new System.Windows.Forms.ImageButton();
            this.imageButtonStop = new System.Windows.Forms.ImageButton();
            this.imageButtonSelect = new System.Windows.Forms.ImageButton();
            this.imageButtonLeft = new System.Windows.Forms.ImageButton();
            this.imageButtonDown = new System.Windows.Forms.ImageButton();
            this.imageButtonRight = new System.Windows.Forms.ImageButton();
            this.imageButtonUp = new System.Windows.Forms.ImageButton();
            this.imageButtonPower = new System.Windows.Forms.ImageButton();
            this.imageButtonRefresh = new System.Windows.Forms.ImageButton();
            this.imageButtonSearch = new System.Windows.Forms.ImageButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroLabelLogLevels = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.metroLabelMac1 = new System.Windows.Forms.Label();
            this.metroLabelMac0 = new System.Windows.Forms.Label();
            this.metroLabelAudio = new System.Windows.Forms.Label();
            this.metroLabelVideo = new System.Windows.Forms.Label();
            this.metroLabelPower = new System.Windows.Forms.Label();
            this.metroLabelBoard = new System.Windows.Forms.Label();
            this.metroLabelXRev = new System.Windows.Forms.Label();
            this.metroLabelFirmware = new System.Windows.Forms.Label();
            this.metroLabelHardware = new System.Windows.Forms.Label();
            this.metroLabelNet1 = new System.Windows.Forms.Label();
            this.metroLabelNet0 = new System.Windows.Forms.Label();
            this.metroLabel8 = new System.Windows.Forms.Label();
            this.metroLabel7 = new System.Windows.Forms.Label();
            this.metroLabel6 = new System.Windows.Forms.Label();
            this.metroLabel5 = new System.Windows.Forms.Label();
            this.metroLabel4 = new System.Windows.Forms.Label();
            this.metroLabel3 = new System.Windows.Forms.Label();
            this.metroLabel2 = new System.Windows.Forms.Label();
            this.metroLabelResult = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelPS = new System.Windows.Forms.Panel();
            this.metroPanelOps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonMute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonVolDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonVolUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonChDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonChUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonCC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPlay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonSearch)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelPS.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanelOps
            // 
            this.metroPanelOps.Controls.Add(this.metroButtonContentUpdate);
            this.metroPanelOps.Controls.Add(this.metroButtonFirmwarePull);
            this.metroPanelOps.Controls.Add(this.metroButtonReboot);
            this.metroPanelOps.Controls.Add(this.metroButtonConsole);
            this.metroPanelOps.Controls.Add(this.metroButtonReload);
            this.metroPanelOps.Location = new System.Drawing.Point(212, 301);
            this.metroPanelOps.Name = "metroPanelOps";
            this.metroPanelOps.Size = new System.Drawing.Size(232, 235);
            this.metroPanelOps.TabIndex = 5;
            // 
            // metroButtonContentUpdate
            // 
            this.metroButtonContentUpdate.Location = new System.Drawing.Point(122, 66);
            this.metroButtonContentUpdate.Name = "metroButtonContentUpdate";
            this.metroButtonContentUpdate.Size = new System.Drawing.Size(100, 23);
            this.metroButtonContentUpdate.TabIndex = 8;
            this.metroButtonContentUpdate.Text = "Content Update";
            // 
            // metroButtonFirmwarePull
            // 
            this.metroButtonFirmwarePull.Location = new System.Drawing.Point(11, 66);
            this.metroButtonFirmwarePull.Name = "metroButtonFirmwarePull";
            this.metroButtonFirmwarePull.Size = new System.Drawing.Size(100, 23);
            this.metroButtonFirmwarePull.TabIndex = 7;
            this.metroButtonFirmwarePull.Text = "Firmware";
            // 
            // metroButtonReboot
            // 
            this.metroButtonReboot.Location = new System.Drawing.Point(122, 28);
            this.metroButtonReboot.Name = "metroButtonReboot";
            this.metroButtonReboot.Size = new System.Drawing.Size(100, 23);
            this.metroButtonReboot.TabIndex = 5;
            this.metroButtonReboot.Text = "Reboot";
            this.metroButtonReboot.Click += new System.EventHandler(this.metroButtonReboot_Click);
            // 
            // metroButtonConsole
            // 
            this.metroButtonConsole.BackgroundImage = global::Prober.Properties.Resources.terminal2;
            this.metroButtonConsole.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.metroButtonConsole.Location = new System.Drawing.Point(122, 186);
            this.metroButtonConsole.Name = "metroButtonConsole";
            this.metroButtonConsole.Size = new System.Drawing.Size(100, 42);
            this.metroButtonConsole.TabIndex = 9;
            this.metroButtonConsole.Text = "Console";
            this.metroButtonConsole.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.metroToolTip1.SetToolTip(this.metroButtonConsole, "Open Debug Console");
            this.metroButtonConsole.Click += new System.EventHandler(this.metroButtonConsole_Click);
            // 
            // metroButtonReload
            // 
            this.metroButtonReload.Location = new System.Drawing.Point(11, 28);
            this.metroButtonReload.Name = "metroButtonReload";
            this.metroButtonReload.Size = new System.Drawing.Size(100, 23);
            this.metroButtonReload.TabIndex = 4;
            this.metroButtonReload.Text = "Reload";
            this.metroButtonReload.Click += new System.EventHandler(this.metroButtonReload_Click);
            // 
            // imageButton0
            // 
            this.imageButton0.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton0.DownImage = null;
            this.imageButton0.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton0.HoverImage")));
            this.imageButton0.Location = new System.Drawing.Point(74, 456);
            this.imageButton0.Name = "imageButton0";
            this.imageButton0.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton0.NormalImage")));
            this.imageButton0.Size = new System.Drawing.Size(28, 28);
            this.imageButton0.TabIndex = 24;
            this.imageButton0.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton0, "0");
            this.imageButton0.Click += new System.EventHandler(this.imageButton0_Click);
            // 
            // metroTextBoxIP
            // 
            this.metroTextBoxIP.BackColor = System.Drawing.Color.White;
            this.metroTextBoxIP.Location = new System.Drawing.Point(15, 75);
            this.metroTextBoxIP.Name = "metroTextBoxIP";
            this.metroTextBoxIP.Size = new System.Drawing.Size(113, 20);
            this.metroTextBoxIP.TabIndex = 0;
            this.metroTextBoxIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.metroToolTip1.SetToolTip(this.metroTextBoxIP, "Enter Dwvice IP or Browse in the database");
            this.metroTextBoxIP.TextChanged += new System.EventHandler(this.metroTextBoxIP_TextChanged);
            this.metroTextBoxIP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.metroTextBoxIP_KeyDown);
            this.metroTextBoxIP.Validating += new System.ComponentModel.CancelEventHandler(this.metroTextBoxIP_Validating);
            // 
            // imageButton9
            // 
            this.imageButton9.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton9.DownImage = null;
            this.imageButton9.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton9.HoverImage")));
            this.imageButton9.Location = new System.Drawing.Point(105, 424);
            this.imageButton9.Name = "imageButton9";
            this.imageButton9.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton9.NormalImage")));
            this.imageButton9.Size = new System.Drawing.Size(28, 28);
            this.imageButton9.TabIndex = 24;
            this.imageButton9.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton9, "9");
            this.imageButton9.Click += new System.EventHandler(this.imageButton9_Click);
            // 
            // imageButton8
            // 
            this.imageButton8.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton8.DownImage = null;
            this.imageButton8.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton8.HoverImage")));
            this.imageButton8.Location = new System.Drawing.Point(74, 424);
            this.imageButton8.Name = "imageButton8";
            this.imageButton8.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton8.NormalImage")));
            this.imageButton8.Size = new System.Drawing.Size(28, 28);
            this.imageButton8.TabIndex = 24;
            this.imageButton8.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton8, "8");
            this.imageButton8.Click += new System.EventHandler(this.imageButton8_Click);
            // 
            // imageButton7
            // 
            this.imageButton7.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton7.DownImage = null;
            this.imageButton7.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton7.HoverImage")));
            this.imageButton7.Location = new System.Drawing.Point(42, 424);
            this.imageButton7.Name = "imageButton7";
            this.imageButton7.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton7.NormalImage")));
            this.imageButton7.Size = new System.Drawing.Size(28, 28);
            this.imageButton7.TabIndex = 24;
            this.imageButton7.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton7, "7");
            this.imageButton7.Click += new System.EventHandler(this.imageButton7_Click);
            // 
            // imageButton6
            // 
            this.imageButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton6.DownImage = null;
            this.imageButton6.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton6.HoverImage")));
            this.imageButton6.Location = new System.Drawing.Point(105, 393);
            this.imageButton6.Name = "imageButton6";
            this.imageButton6.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton6.NormalImage")));
            this.imageButton6.Size = new System.Drawing.Size(28, 28);
            this.imageButton6.TabIndex = 24;
            this.imageButton6.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton6, "6");
            this.imageButton6.Click += new System.EventHandler(this.imageButton6_Click);
            // 
            // imageButton5
            // 
            this.imageButton5.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton5.DownImage = null;
            this.imageButton5.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton5.HoverImage")));
            this.imageButton5.Location = new System.Drawing.Point(74, 393);
            this.imageButton5.Name = "imageButton5";
            this.imageButton5.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton5.NormalImage")));
            this.imageButton5.Size = new System.Drawing.Size(28, 28);
            this.imageButton5.TabIndex = 24;
            this.imageButton5.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton5, "5");
            this.imageButton5.Click += new System.EventHandler(this.imageButton5_Click);
            // 
            // imageButton4
            // 
            this.imageButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton4.DownImage = null;
            this.imageButton4.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton4.HoverImage")));
            this.imageButton4.Location = new System.Drawing.Point(42, 393);
            this.imageButton4.Name = "imageButton4";
            this.imageButton4.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton4.NormalImage")));
            this.imageButton4.Size = new System.Drawing.Size(28, 28);
            this.imageButton4.TabIndex = 23;
            this.imageButton4.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton4, "4");
            this.imageButton4.Click += new System.EventHandler(this.imageButton4_Click);
            // 
            // imageButton3
            // 
            this.imageButton3.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton3.DownImage = null;
            this.imageButton3.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton3.HoverImage")));
            this.imageButton3.Location = new System.Drawing.Point(105, 361);
            this.imageButton3.Name = "imageButton3";
            this.imageButton3.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton3.NormalImage")));
            this.imageButton3.Size = new System.Drawing.Size(28, 28);
            this.imageButton3.TabIndex = 23;
            this.imageButton3.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton3, "3");
            this.imageButton3.Click += new System.EventHandler(this.imageButton3_Click);
            // 
            // imageButton2
            // 
            this.imageButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton2.DownImage = null;
            this.imageButton2.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton2.HoverImage")));
            this.imageButton2.Location = new System.Drawing.Point(74, 361);
            this.imageButton2.Name = "imageButton2";
            this.imageButton2.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton2.NormalImage")));
            this.imageButton2.Size = new System.Drawing.Size(28, 28);
            this.imageButton2.TabIndex = 22;
            this.imageButton2.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton2, "2");
            this.imageButton2.Click += new System.EventHandler(this.imageButton2_Click);
            // 
            // imageButton1
            // 
            this.imageButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton1.DownImage = null;
            this.imageButton1.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButton1.HoverImage")));
            this.imageButton1.Location = new System.Drawing.Point(42, 361);
            this.imageButton1.Name = "imageButton1";
            this.imageButton1.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton1.NormalImage")));
            this.imageButton1.Size = new System.Drawing.Size(28, 28);
            this.imageButton1.TabIndex = 21;
            this.imageButton1.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButton1, "1");
            this.imageButton1.Click += new System.EventHandler(this.imageButton1_Click);
            // 
            // imageButtonMute
            // 
            this.imageButtonMute.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonMute.DownImage = null;
            this.imageButtonMute.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonMute.HoverImage")));
            this.imageButtonMute.Location = new System.Drawing.Point(74, 329);
            this.imageButtonMute.Name = "imageButtonMute";
            this.imageButtonMute.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonMute.NormalImage")));
            this.imageButtonMute.Size = new System.Drawing.Size(28, 28);
            this.imageButtonMute.TabIndex = 20;
            this.imageButtonMute.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonMute, "Mute");
            this.imageButtonMute.Click += new System.EventHandler(this.imageButtonMute_Click);
            // 
            // imageButtonPrev
            // 
            this.imageButtonPrev.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonPrev.DownImage = null;
            this.imageButtonPrev.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPrev.HoverImage")));
            this.imageButtonPrev.Location = new System.Drawing.Point(74, 297);
            this.imageButtonPrev.Name = "imageButtonPrev";
            this.imageButtonPrev.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPrev.NormalImage")));
            this.imageButtonPrev.Size = new System.Drawing.Size(28, 28);
            this.imageButtonPrev.TabIndex = 19;
            this.imageButtonPrev.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonPrev, "Previous");
            this.imageButtonPrev.Click += new System.EventHandler(this.imageButtonPrev_Click);
            // 
            // imageButtonVolDown
            // 
            this.imageButtonVolDown.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonVolDown.DownImage = null;
            this.imageButtonVolDown.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonVolDown.HoverImage")));
            this.imageButtonVolDown.Location = new System.Drawing.Point(108, 329);
            this.imageButtonVolDown.Name = "imageButtonVolDown";
            this.imageButtonVolDown.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonVolDown.NormalImage")));
            this.imageButtonVolDown.Size = new System.Drawing.Size(28, 28);
            this.imageButtonVolDown.TabIndex = 18;
            this.imageButtonVolDown.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonVolDown, "Vol -");
            this.imageButtonVolDown.Click += new System.EventHandler(this.imageButtonVolDown_Click);
            // 
            // imageButtonVolUp
            // 
            this.imageButtonVolUp.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonVolUp.DownImage = null;
            this.imageButtonVolUp.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonVolUp.HoverImage")));
            this.imageButtonVolUp.Location = new System.Drawing.Point(108, 297);
            this.imageButtonVolUp.Name = "imageButtonVolUp";
            this.imageButtonVolUp.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonVolUp.NormalImage")));
            this.imageButtonVolUp.Size = new System.Drawing.Size(28, 28);
            this.imageButtonVolUp.TabIndex = 17;
            this.imageButtonVolUp.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonVolUp, "Vol +");
            this.imageButtonVolUp.Click += new System.EventHandler(this.imageButtonVolUp_Click);
            // 
            // imageButtonChDown
            // 
            this.imageButtonChDown.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonChDown.DownImage = null;
            this.imageButtonChDown.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonChDown.HoverImage")));
            this.imageButtonChDown.Location = new System.Drawing.Point(40, 329);
            this.imageButtonChDown.Name = "imageButtonChDown";
            this.imageButtonChDown.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonChDown.NormalImage")));
            this.imageButtonChDown.Size = new System.Drawing.Size(28, 28);
            this.imageButtonChDown.TabIndex = 16;
            this.imageButtonChDown.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonChDown, "CH -");
            this.imageButtonChDown.Click += new System.EventHandler(this.imageButtonChDown_Click);
            // 
            // imageButtonChUp
            // 
            this.imageButtonChUp.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonChUp.DownImage = null;
            this.imageButtonChUp.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonChUp.HoverImage")));
            this.imageButtonChUp.Location = new System.Drawing.Point(40, 297);
            this.imageButtonChUp.Name = "imageButtonChUp";
            this.imageButtonChUp.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonChUp.NormalImage")));
            this.imageButtonChUp.Size = new System.Drawing.Size(28, 28);
            this.imageButtonChUp.TabIndex = 15;
            this.imageButtonChUp.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonChUp, "CH +");
            this.imageButtonChUp.Click += new System.EventHandler(this.imageButtonChUp_Click);
            // 
            // imageButtonMenu
            // 
            this.imageButtonMenu.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonMenu.DownImage = null;
            this.imageButtonMenu.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonMenu.HoverImage")));
            this.imageButtonMenu.Location = new System.Drawing.Point(108, 265);
            this.imageButtonMenu.Name = "imageButtonMenu";
            this.imageButtonMenu.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonMenu.NormalImage")));
            this.imageButtonMenu.Size = new System.Drawing.Size(25, 25);
            this.imageButtonMenu.TabIndex = 14;
            this.imageButtonMenu.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonMenu, "Menu");
            this.imageButtonMenu.Click += new System.EventHandler(this.imageButtonMenu_Click);
            // 
            // imageButtonCC
            // 
            this.imageButtonCC.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonCC.DownImage = null;
            this.imageButtonCC.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonCC.HoverImage")));
            this.imageButtonCC.Location = new System.Drawing.Point(42, 264);
            this.imageButtonCC.Name = "imageButtonCC";
            this.imageButtonCC.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonCC.NormalImage")));
            this.imageButtonCC.Size = new System.Drawing.Size(25, 25);
            this.imageButtonCC.TabIndex = 13;
            this.imageButtonCC.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonCC, "Closed Caption");
            this.imageButtonCC.Click += new System.EventHandler(this.imageButtonCC_Click);
            // 
            // imageButtonPlay
            // 
            this.imageButtonPlay.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonPlay.DownImage = null;
            this.imageButtonPlay.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPlay.HoverImage")));
            this.imageButtonPlay.Location = new System.Drawing.Point(108, 211);
            this.imageButtonPlay.Name = "imageButtonPlay";
            this.imageButtonPlay.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPlay.NormalImage")));
            this.imageButtonPlay.Size = new System.Drawing.Size(25, 25);
            this.imageButtonPlay.TabIndex = 12;
            this.imageButtonPlay.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonPlay, "Play / Pause");
            this.imageButtonPlay.Click += new System.EventHandler(this.imageButtonPlay_Click);
            // 
            // imageButtonStop
            // 
            this.imageButtonStop.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonStop.DownImage = null;
            this.imageButtonStop.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonStop.HoverImage")));
            this.imageButtonStop.Location = new System.Drawing.Point(42, 211);
            this.imageButtonStop.Name = "imageButtonStop";
            this.imageButtonStop.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonStop.NormalImage")));
            this.imageButtonStop.Size = new System.Drawing.Size(25, 25);
            this.imageButtonStop.TabIndex = 11;
            this.imageButtonStop.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonStop, "Stop");
            this.imageButtonStop.Click += new System.EventHandler(this.imageButtonStop_Click);
            // 
            // imageButtonSelect
            // 
            this.imageButtonSelect.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonSelect.DownImage = null;
            this.imageButtonSelect.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonSelect.HoverImage")));
            this.imageButtonSelect.Location = new System.Drawing.Point(74, 236);
            this.imageButtonSelect.Name = "imageButtonSelect";
            this.imageButtonSelect.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonSelect.NormalImage")));
            this.imageButtonSelect.Size = new System.Drawing.Size(28, 28);
            this.imageButtonSelect.TabIndex = 10;
            this.imageButtonSelect.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonSelect, "Select");
            this.imageButtonSelect.Click += new System.EventHandler(this.imageButtonSelect_Click);
            // 
            // imageButtonLeft
            // 
            this.imageButtonLeft.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonLeft.DownImage = null;
            this.imageButtonLeft.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonLeft.HoverImage")));
            this.imageButtonLeft.Location = new System.Drawing.Point(41, 233);
            this.imageButtonLeft.Name = "imageButtonLeft";
            this.imageButtonLeft.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonLeft.NormalImage")));
            this.imageButtonLeft.Size = new System.Drawing.Size(32, 34);
            this.imageButtonLeft.TabIndex = 9;
            this.imageButtonLeft.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonLeft, "Left");
            this.imageButtonLeft.Click += new System.EventHandler(this.imageButtonLeft_Click);
            // 
            // imageButtonDown
            // 
            this.imageButtonDown.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonDown.DownImage = null;
            this.imageButtonDown.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonDown.HoverImage")));
            this.imageButtonDown.Location = new System.Drawing.Point(70, 266);
            this.imageButtonDown.Name = "imageButtonDown";
            this.imageButtonDown.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonDown.NormalImage")));
            this.imageButtonDown.Size = new System.Drawing.Size(34, 32);
            this.imageButtonDown.TabIndex = 8;
            this.imageButtonDown.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonDown, "Down");
            this.imageButtonDown.Click += new System.EventHandler(this.imageButtonDown_Click);
            // 
            // imageButtonRight
            // 
            this.imageButtonRight.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonRight.DownImage = null;
            this.imageButtonRight.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonRight.HoverImage")));
            this.imageButtonRight.Location = new System.Drawing.Point(103, 233);
            this.imageButtonRight.Name = "imageButtonRight";
            this.imageButtonRight.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonRight.NormalImage")));
            this.imageButtonRight.Size = new System.Drawing.Size(32, 34);
            this.imageButtonRight.TabIndex = 7;
            this.imageButtonRight.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonRight, "Right");
            this.imageButtonRight.Click += new System.EventHandler(this.imageButtonRight_Click);
            // 
            // imageButtonUp
            // 
            this.imageButtonUp.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonUp.DownImage = null;
            this.imageButtonUp.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonUp.HoverImage")));
            this.imageButtonUp.Location = new System.Drawing.Point(70, 204);
            this.imageButtonUp.Name = "imageButtonUp";
            this.imageButtonUp.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonUp.NormalImage")));
            this.imageButtonUp.Size = new System.Drawing.Size(34, 32);
            this.imageButtonUp.TabIndex = 6;
            this.imageButtonUp.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonUp, "Up");
            this.imageButtonUp.Click += new System.EventHandler(this.imageButtonUp_Click);
            // 
            // imageButtonPower
            // 
            this.imageButtonPower.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonPower.DownImage = null;
            this.imageButtonPower.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPower.HoverImage")));
            this.imageButtonPower.Location = new System.Drawing.Point(74, 170);
            this.imageButtonPower.Name = "imageButtonPower";
            this.imageButtonPower.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonPower.NormalImage")));
            this.imageButtonPower.Size = new System.Drawing.Size(25, 25);
            this.imageButtonPower.TabIndex = 5;
            this.imageButtonPower.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonPower, "TV Power Toggle");
            this.imageButtonPower.Click += new System.EventHandler(this.imageButtonPower_Click);
            // 
            // imageButtonRefresh
            // 
            this.imageButtonRefresh.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonRefresh.DownImage = null;
            this.imageButtonRefresh.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonRefresh.HoverImage")));
            this.imageButtonRefresh.Location = new System.Drawing.Point(59, 104);
            this.imageButtonRefresh.Name = "imageButtonRefresh";
            this.imageButtonRefresh.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonRefresh.NormalImage")));
            this.imageButtonRefresh.Size = new System.Drawing.Size(56, 56);
            this.imageButtonRefresh.TabIndex = 4;
            this.imageButtonRefresh.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonRefresh, "Refresh STB status");
            this.imageButtonRefresh.Click += new System.EventHandler(this.imageButtonRefresh_Click);
            // 
            // imageButtonSearch
            // 
            this.imageButtonSearch.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButtonSearch.DownImage = null;
            this.imageButtonSearch.HoverImage = ((System.Drawing.Image)(resources.GetObject("imageButtonSearch.HoverImage")));
            this.imageButtonSearch.Location = new System.Drawing.Point(134, 75);
            this.imageButtonSearch.Name = "imageButtonSearch";
            this.imageButtonSearch.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButtonSearch.NormalImage")));
            this.imageButtonSearch.Size = new System.Drawing.Size(25, 25);
            this.imageButtonSearch.TabIndex = 3;
            this.imageButtonSearch.TabStop = false;
            this.metroToolTip1.SetToolTip(this.imageButtonSearch, "Browse STB Database");
            this.imageButtonSearch.Click += new System.EventHandler(this.imageButtonSearch_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.metroLabelLogLevels);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.metroLabelMac1);
            this.groupBox1.Controls.Add(this.metroLabelMac0);
            this.groupBox1.Controls.Add(this.metroLabelAudio);
            this.groupBox1.Controls.Add(this.metroLabelVideo);
            this.groupBox1.Controls.Add(this.metroLabelPower);
            this.groupBox1.Controls.Add(this.metroLabelBoard);
            this.groupBox1.Controls.Add(this.metroLabelXRev);
            this.groupBox1.Controls.Add(this.metroLabelFirmware);
            this.groupBox1.Controls.Add(this.metroLabelHardware);
            this.groupBox1.Controls.Add(this.metroLabelNet1);
            this.groupBox1.Controls.Add(this.metroLabelNet0);
            this.groupBox1.Controls.Add(this.metroLabel8);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Location = new System.Drawing.Point(212, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(232, 264);
            this.groupBox1.TabIndex = 100;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // metroLabelLogLevels
            // 
            this.metroLabelLogLevels.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelLogLevels.Location = new System.Drawing.Point(92, 187);
            this.metroLabelLogLevels.Name = "metroLabelLogLevels";
            this.metroLabelLogLevels.Size = new System.Drawing.Size(134, 23);
            this.metroLabelLogLevels.TabIndex = 20;
            this.metroLabelLogLevels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(6, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "Log Levels :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabelMac1
            // 
            this.metroLabelMac1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelMac1.Location = new System.Drawing.Point(92, 242);
            this.metroLabelMac1.Name = "metroLabelMac1";
            this.metroLabelMac1.Size = new System.Drawing.Size(134, 23);
            this.metroLabelMac1.TabIndex = 18;
            this.metroLabelMac1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelMac0
            // 
            this.metroLabelMac0.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelMac0.Location = new System.Drawing.Point(92, 219);
            this.metroLabelMac0.Name = "metroLabelMac0";
            this.metroLabelMac0.Size = new System.Drawing.Size(134, 23);
            this.metroLabelMac0.TabIndex = 17;
            this.metroLabelMac0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelAudio
            // 
            this.metroLabelAudio.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelAudio.Location = new System.Drawing.Point(92, 164);
            this.metroLabelAudio.Name = "metroLabelAudio";
            this.metroLabelAudio.Size = new System.Drawing.Size(134, 23);
            this.metroLabelAudio.TabIndex = 16;
            this.metroLabelAudio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelVideo
            // 
            this.metroLabelVideo.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelVideo.Location = new System.Drawing.Point(92, 141);
            this.metroLabelVideo.Name = "metroLabelVideo";
            this.metroLabelVideo.Size = new System.Drawing.Size(134, 23);
            this.metroLabelVideo.TabIndex = 15;
            this.metroLabelVideo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelPower
            // 
            this.metroLabelPower.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelPower.Location = new System.Drawing.Point(92, 118);
            this.metroLabelPower.Name = "metroLabelPower";
            this.metroLabelPower.Size = new System.Drawing.Size(134, 23);
            this.metroLabelPower.TabIndex = 13;
            this.metroLabelPower.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelBoard
            // 
            this.metroLabelBoard.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelBoard.Location = new System.Drawing.Point(92, 95);
            this.metroLabelBoard.Name = "metroLabelBoard";
            this.metroLabelBoard.Size = new System.Drawing.Size(134, 23);
            this.metroLabelBoard.TabIndex = 12;
            this.metroLabelBoard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelXRev
            // 
            this.metroLabelXRev.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelXRev.Location = new System.Drawing.Point(92, 72);
            this.metroLabelXRev.Name = "metroLabelXRev";
            this.metroLabelXRev.Size = new System.Drawing.Size(134, 23);
            this.metroLabelXRev.TabIndex = 11;
            this.metroLabelXRev.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelFirmware
            // 
            this.metroLabelFirmware.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelFirmware.Location = new System.Drawing.Point(92, 49);
            this.metroLabelFirmware.Name = "metroLabelFirmware";
            this.metroLabelFirmware.Size = new System.Drawing.Size(134, 23);
            this.metroLabelFirmware.TabIndex = 10;
            this.metroLabelFirmware.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelHardware
            // 
            this.metroLabelHardware.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelHardware.Location = new System.Drawing.Point(92, 26);
            this.metroLabelHardware.Name = "metroLabelHardware";
            this.metroLabelHardware.Size = new System.Drawing.Size(134, 23);
            this.metroLabelHardware.TabIndex = 9;
            this.metroLabelHardware.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabelNet1
            // 
            this.metroLabelNet1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelNet1.Location = new System.Drawing.Point(6, 242);
            this.metroLabelNet1.Name = "metroLabelNet1";
            this.metroLabelNet1.Size = new System.Drawing.Size(80, 23);
            this.metroLabelNet1.TabIndex = 8;
            this.metroLabelNet1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabelNet0
            // 
            this.metroLabelNet0.BackColor = System.Drawing.Color.Transparent;
            this.metroLabelNet0.Location = new System.Drawing.Point(6, 219);
            this.metroLabelNet0.Name = "metroLabelNet0";
            this.metroLabelNet0.Size = new System.Drawing.Size(80, 23);
            this.metroLabelNet0.TabIndex = 7;
            this.metroLabelNet0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel8
            // 
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.Location = new System.Drawing.Point(6, 164);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(80, 23);
            this.metroLabel8.TabIndex = 6;
            this.metroLabel8.Text = "Audio Out :";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel7
            // 
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.Location = new System.Drawing.Point(6, 141);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(80, 23);
            this.metroLabel7.TabIndex = 5;
            this.metroLabel7.Text = "Video Out :";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel6
            // 
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.Location = new System.Drawing.Point(6, 118);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(80, 23);
            this.metroLabel6.TabIndex = 4;
            this.metroLabel6.Text = "Power :";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel5
            // 
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.Location = new System.Drawing.Point(6, 95);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(80, 23);
            this.metroLabel5.TabIndex = 3;
            this.metroLabel5.Text = "Board ID :";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel4
            // 
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.Location = new System.Drawing.Point(6, 72);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(80, 23);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "X Rev :";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel3
            // 
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.Location = new System.Drawing.Point(6, 49);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(80, 23);
            this.metroLabel3.TabIndex = 1;
            this.metroLabel3.Text = "Firmware :";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel2
            // 
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Location = new System.Drawing.Point(6, 26);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(80, 23);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Hardware :";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabelResult
            // 
            this.metroLabelResult.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroLabelResult.AutoSize = true;
            this.metroLabelResult.BackColor = System.Drawing.Color.LemonChiffon;
            this.metroLabelResult.Location = new System.Drawing.Point(163, 24);
            this.metroLabelResult.Name = "metroLabelResult";
            this.metroLabelResult.Size = new System.Drawing.Size(79, 13);
            this.metroLabelResult.TabIndex = 101;
            this.metroLabelResult.Text = "Command Sent";
            this.metroLabelResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabelResult.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(456, 24);
            this.menuStrip1.TabIndex = 102;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openLogFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openLogFileToolStripMenuItem
            // 
            this.openLogFileToolStripMenuItem.Name = "openLogFileToolStripMenuItem";
            this.openLogFileToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.openLogFileToolStripMenuItem.Text = "Open log file";
            this.openLogFileToolStripMenuItem.Click += new System.EventHandler(this.openLogFileToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // panelPS
            // 
            this.panelPS.BackColor = System.Drawing.Color.Transparent;
            this.panelPS.BackgroundImage = global::Prober.Properties.Resources.ps;
            this.panelPS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelPS.Controls.Add(this.imageButton0);
            this.panelPS.Controls.Add(this.metroTextBoxIP);
            this.panelPS.Controls.Add(this.imageButton9);
            this.panelPS.Controls.Add(this.imageButton8);
            this.panelPS.Controls.Add(this.imageButton7);
            this.panelPS.Controls.Add(this.imageButton6);
            this.panelPS.Controls.Add(this.imageButton5);
            this.panelPS.Controls.Add(this.imageButton4);
            this.panelPS.Controls.Add(this.imageButton3);
            this.panelPS.Controls.Add(this.imageButton2);
            this.panelPS.Controls.Add(this.imageButton1);
            this.panelPS.Controls.Add(this.imageButtonMute);
            this.panelPS.Controls.Add(this.imageButtonPrev);
            this.panelPS.Controls.Add(this.imageButtonVolDown);
            this.panelPS.Controls.Add(this.imageButtonVolUp);
            this.panelPS.Controls.Add(this.imageButtonChDown);
            this.panelPS.Controls.Add(this.imageButtonChUp);
            this.panelPS.Controls.Add(this.imageButtonMenu);
            this.panelPS.Controls.Add(this.imageButtonCC);
            this.panelPS.Controls.Add(this.imageButtonPlay);
            this.panelPS.Controls.Add(this.imageButtonStop);
            this.panelPS.Controls.Add(this.imageButtonSelect);
            this.panelPS.Controls.Add(this.imageButtonLeft);
            this.panelPS.Controls.Add(this.imageButtonDown);
            this.panelPS.Controls.Add(this.imageButtonRight);
            this.panelPS.Controls.Add(this.imageButtonUp);
            this.panelPS.Controls.Add(this.imageButtonPower);
            this.panelPS.Controls.Add(this.imageButtonRefresh);
            this.panelPS.Controls.Add(this.imageButtonSearch);
            this.panelPS.Location = new System.Drawing.Point(13, 31);
            this.panelPS.Name = "panelPS";
            this.panelPS.Size = new System.Drawing.Size(178, 505);
            this.panelPS.TabIndex = 99;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 544);
            this.Controls.Add(this.metroLabelResult);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelPS);
            this.Controls.Add(this.metroPanelOps);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Aceso Prober";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.metroPanelOps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageButton0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonMute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonVolDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonVolUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonChDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonChUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonCC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPlay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButtonSearch)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelPS.ResumeLayout(false);
            this.panelPS.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelPS;
        private System.Windows.Forms.ImageButton imageButtonSearch;
        private System.Windows.Forms.ImageButton imageButtonRefresh;
        private System.Windows.Forms.ImageButton imageButtonDown;
        private System.Windows.Forms.ImageButton imageButtonRight;
        private System.Windows.Forms.ImageButton imageButtonUp;
        private System.Windows.Forms.ImageButton imageButtonPower;
        private System.Windows.Forms.ImageButton imageButtonSelect;
        private System.Windows.Forms.ImageButton imageButtonLeft;
        private System.Windows.Forms.ImageButton imageButtonStop;
        private System.Windows.Forms.ImageButton imageButtonCC;
        private System.Windows.Forms.ImageButton imageButtonPlay;
        private System.Windows.Forms.ImageButton imageButton1;
        private System.Windows.Forms.ImageButton imageButtonMute;
        private System.Windows.Forms.ImageButton imageButtonPrev;
        private System.Windows.Forms.ImageButton imageButtonVolDown;
        private System.Windows.Forms.ImageButton imageButtonVolUp;
        private System.Windows.Forms.ImageButton imageButtonChDown;
        private System.Windows.Forms.ImageButton imageButtonChUp;
        private System.Windows.Forms.ImageButton imageButtonMenu;
        private System.Windows.Forms.ImageButton imageButton0;
        private System.Windows.Forms.ImageButton imageButton9;
        private System.Windows.Forms.ImageButton imageButton8;
        private System.Windows.Forms.ImageButton imageButton7;
        private System.Windows.Forms.ImageButton imageButton6;
        private System.Windows.Forms.ImageButton imageButton5;
        private System.Windows.Forms.ImageButton imageButton4;
        private System.Windows.Forms.ImageButton imageButton3;
        private System.Windows.Forms.ImageButton imageButton2;
        private System.Windows.Forms.Panel metroPanelOps;
        private System.Windows.Forms.ToolTip metroToolTip1;
        private System.Windows.Forms.TextBox metroTextBoxIP;
        private System.Windows.Forms.Button metroButtonReboot;
        private System.Windows.Forms.Button metroButtonReload;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label metroLabel2;
        private System.Windows.Forms.Label metroLabel4;
        private System.Windows.Forms.Label metroLabel3;
        private System.Windows.Forms.Label metroLabel5;
        private System.Windows.Forms.Label metroLabel7;
        private System.Windows.Forms.Label metroLabel6;
        private System.Windows.Forms.Label metroLabel8;
        private System.Windows.Forms.Label metroLabelXRev;
        private System.Windows.Forms.Label metroLabelFirmware;
        private System.Windows.Forms.Label metroLabelHardware;
        private System.Windows.Forms.Label metroLabelNet1;
        private System.Windows.Forms.Label metroLabelNet0;
        private System.Windows.Forms.Label metroLabelBoard;
        private System.Windows.Forms.Label metroLabelMac1;
        private System.Windows.Forms.Label metroLabelMac0;
        private System.Windows.Forms.Label metroLabelAudio;
        private System.Windows.Forms.Label metroLabelVideo;
        private System.Windows.Forms.Label metroLabelPower;
        private System.Windows.Forms.Button metroButtonFirmwarePull;
        private System.Windows.Forms.Button metroButtonContentUpdate;
        private System.Windows.Forms.Button metroButtonConsole;
        private System.Windows.Forms.Label metroLabelResult;
        private System.Windows.Forms.Label metroLabelLogLevels;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogFileToolStripMenuItem;
    }
}

