﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Prober.Common
{
    public class DeviceDetector
    {
        public static readonly int EnseoPort = 27275;
        public static readonly int LgPort = 9001;
        public static readonly int ConnectTimeout = 2000;

        public static DeviceType Detect(string ip, string mac = null)
        {
            if (IsEnseo(ip, mac))
            {
                return DeviceType.Enseo;
            }
            else if (IsProcentric(ip, mac))
            {
                return DeviceType.Procentric;
            }

            return DeviceType.Unknown;
        }

        public static bool IsEnseo(string ip, string mac)
        {
            if (!string.IsNullOrEmpty(mac))
            {
                mac = mac.ToUpper();
                if (mac.StartsWith("0021F8"))
                    return true;
            }
            if (!string.IsNullOrEmpty(ip))
            {
                using (var client = new TcpClient())
                {
                    try
                    {
                        var result = client.BeginConnect(ip, EnseoPort, null, null);
                        var success = result.AsyncWaitHandle.WaitOne(ConnectTimeout);
                        if (success)
                        {
                            client.EndConnect(result);
                            return true;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public static bool IsProcentric(string ip, string mac)
        {
            if (!string.IsNullOrEmpty(ip))
            {
                using (var client = new TcpClient())
                {
                    try
                    {
                        var result = client.BeginConnect(ip, LgPort, null, null);
                        var success = result.AsyncWaitHandle.WaitOne(ConnectTimeout);
                        if (success)
                        {
                            client.EndConnect(result);
                            return true;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            return false;
        }
    }
}
