﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBOutputSettings
    {
        public string Connector;
        public string TVStandard;
        public string ColorSpace;
        public string AspectRatio;
        public string ScalingMode;
        public bool AutoHDMI;
        public bool AutoHDMIOnly;

        public override string ToString()
        {
            return string.Format("{1}{0}{2}",
                Environment.NewLine, Connector, TVStandard);
        }
    }
}
