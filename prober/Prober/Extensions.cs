﻿// -----------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Hewlett-Packard">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Prober
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class StringExtension
    {
        public static bool IsNullOrWhiteSpace(String value)
        {
            if (value == null) return true;

            for (int i = 0; i < value.Length; i++)
            {
                if (!Char.IsWhiteSpace(value[i])) return false;
            }

            return true;
        }
    }
}
