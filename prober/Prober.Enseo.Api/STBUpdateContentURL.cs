﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Prober.Enseo.Api
{
    public struct STBUpdateContentURL
    {
        public string Location;
        public int Port;
        public string TransferProtocol;
        public string Username;
        public string Password;
        public string UpdateFile;
        public string UpdateFolder;
        public string UpdateDrive;
        public string AutoPlay;

        public override string ToString()
        {
            string filepath = Path.Combine(UpdateFolder, UpdateFile);
            return string.Format("{0}://{1}:{2}@{3}:{4}/{5}",
                TransferProtocol, Username, Password, Location, Port, filepath);
        }
    }
}
