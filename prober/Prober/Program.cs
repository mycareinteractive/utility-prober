﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Prober
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length >= 2)
            {
                var form = new MainForm();
                bool ret = false;
                if (string.Compare(args[0], "-reloadui", true) == 0)
                {
                    ret = form.ReloadUI(args[1]);
                }
                else if(string.Compare(args[0], "-reboot", true) == 0)
                {
                    ret = form.Reboot(args[1]);
                }
                if (!ret)
                {
                    Environment.Exit(1);
                }
                return;
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
