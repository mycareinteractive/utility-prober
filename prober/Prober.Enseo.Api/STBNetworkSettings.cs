﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBNetworkSettings
    {
        public bool UseDHCP { get; set; }
        public string Address { get; set; }
        public string Netmask { get; set; }
        public string Gateway { get; set; }
        public string DNS1 { get; set; }
        public string DNS2 { get; set; }
        public string DNS3 { get; set; }

        public override string ToString()
        {
            return string.Format("UseDHCP:{1}{0}IP:{2}{0}Mask:{3}{0}Gateway:{4}{0}DNS1:{5}{0}DNS2:{6}{0}DNS3:{7}", 
                Environment.NewLine, UseDHCP, Address, Netmask, Gateway, DNS1, DNS2, DNS3);
        }
    }
}
