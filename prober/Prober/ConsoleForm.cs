﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MinimalisticTelnet;
using System.Threading;
using System.Runtime.InteropServices;
using System.Net;
using Prober.Common;
using Prober.Lg.Api;

namespace Prober
{

    public partial class ConsoleForm : Form
    {
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);
        const int WM_USER = 0x400;
        const int EM_HIDESELECTION = WM_USER + 63;


        public delegate void OutputDelegate(string message);
        public OutputDelegate WriteOutput;

        public TelnetConnection Telnet { get; private set; }
        
        private LgClient _lgApi;
        private ILogger _logger = null;
        private Logger _consoleLog = null;
        private string _address = null;
        private DeviceType _type = DeviceType.None;
        private Thread _telnetThread = null;

        public ConsoleForm(ILogger logger)
        {
            InitializeComponent();
            _logger = logger;
            _consoleLog = new Logger("console.log", 200);
            _lgApi = new LgClient(_logger);
            WriteOutput = new OutputDelegate(OutputCore);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // clean up
                this.Stop();
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public void Start(String address, DeviceType type)
        {
            this.richTextBoxOutput.Clear();

            _logger.Log(string.Format("Starting console for {0}, device {1}", address, type.ToString()));

            IPAddress addr;
            if (!IPAddress.TryParse(address, out addr))
            {
                MessageBox.Show("Invalid IP Address", "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            _address = address;
            _type = type;

            if (_type == DeviceType.Procentric)
            {
                // LG needs to turn on logging first
                _lgApi.LogInitialize(_address);
            }

            try
            {
                //create a new telnet connection
                Telnet = new TelnetConnection(_address, 23);

                if (type == DeviceType.Enseo)
                {
                    string s = Telnet.Login("root", "calica", 1000);
                    Telnet.WriteLine("/usr/bin/get_console inout");
                }
                else if(type == DeviceType.Procentric)
                {
                    string s = Telnet.Login("rms", "", 2000);
                }

                _telnetThread = new Thread(TelnetReadThreadProc);
                _telnetThread.Start(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        public void Stop()
        {
            if (this.Telnet != null)
            {
                if (_type == DeviceType.Enseo)
                {
                    // send ctrl-c
                    Telnet.Write(Convert.ToString((Char)0x03));
                    String bye = Telnet.Read();
                }
                // stop telnet
                Telnet.Close();
            }
            if (this._telnetThread != null)
            {
                // wait for thread to stop
                _telnetThread.Join(1000);
                if (_telnetThread.IsAlive)
                {
                    _telnetThread.Abort();
                }
                _telnetThread = null;
            }
            Telnet = null;

            if (_type == DeviceType.Procentric)
            {
                _lgApi.LogFinalize(_address);
            }
        }

        private void Output(String message)
        {
            if (this.richTextBoxOutput.InvokeRequired)
            {
                this.Invoke(this.WriteOutput, message);
            }
            else
            {
                this.WriteOutput.Invoke(message);
            }
        }

        public void HideSelection(bool hide)
        {
            SendMessage(this.richTextBoxOutput.Handle, EM_HIDESELECTION, hide ? 1 : 0, 0);
        }

        public void OutputCore(String text)
        {
            if (this.richTextBoxOutput.Focused)
            {
                if (this.scrollCheckBox.Checked)
                {
                    HideSelection(false);
                    this.richTextBoxOutput.AppendText(text);
                }
                else
                {
                    HideSelection(true);
                    int pos = this.richTextBoxOutput.SelectionStart;
                    int len = this.richTextBoxOutput.SelectionLength;
                    this.richTextBoxOutput.AppendText(text);
                    this.richTextBoxOutput.SelectionStart = pos;
                    this.richTextBoxOutput.SelectionLength = len;
                    HideSelection(false);
                    this.richTextBoxOutput.Focus();

                }

            }
            else
            {
                if (this.scrollCheckBox.Checked)
                {
                    HideSelection(false);
                    this.richTextBoxOutput.AppendText(text);
                }
                else
                {
                    HideSelection(true);
                    int pos = this.richTextBoxOutput.SelectionStart;
                    this.richTextBoxOutput.AppendText(text);
                    this.richTextBoxOutput.SelectionStart = pos;
                }
            }
        }

        public static void TelnetReadThreadProc(object data)
        {
            ConsoleForm console = (ConsoleForm)data;
            TelnetConnection telnet = console.Telnet;
            try
            {
                while (telnet.IsConnected)
                {
                    String msg = telnet.Read();
                    if (msg == null)
                    {
                        break;
                    }
                    console.Output(msg);
                    console._consoleLog.LogRaw(msg);
                }
            }
            catch
            {

            }
            finally
            {
            }
        }


        private void ConsoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Visible)
            {
                e.Cancel = true;
            }
            this.Stop();
            this.Hide();
        }

        private void richTextBoxOutput_KeyPress(object sender, KeyPressEventArgs e)
        {
            String keyString = e.KeyChar.ToString();
            if (this.Telnet != null && this.Telnet.IsConnected)
            {
                this.Telnet.Write(keyString);
                _consoleLog.LogRaw(keyString);
            }
        }

        private void scrollCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.scrollCheckBox.Text = (this.scrollCheckBox.Checked) ? "Scroll ON" : "Scroll OFF";
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.richTextBoxOutput.Text);
        }

        private void openLogButton_Click(object sender, EventArgs e)
        {
            string path = _consoleLog.GetFilePath();
            System.Diagnostics.Process.Start(path);
        }


    }
}
