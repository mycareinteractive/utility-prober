﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Prober.Lg.Api
{
    public interface IRmsXml
    {
        void ToXml(XmlWriter writer);

        void FromXml(XmlReader reader);
    }
}
