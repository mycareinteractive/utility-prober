﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Common
{
    /// <summary>
    /// A string represent the MAC address of the STB
    /// </summary>
    public struct DeviceMacAddress
    {
        private const string _invalid = "000000000000";
        private string _address;

        public DeviceMacAddress(byte[] addr)
        {
            _address = "";
            if (addr.Length == 6)
            {
                foreach (byte b in addr)
                    _address += b.ToString("X2");
            }
            else
            {
                _address = _invalid;
            }
        }

        public DeviceMacAddress(string addr)
        {
            _address = _invalid;
            string macStr = addr.Replace(":", "").Replace(".", "").Replace("-", "").ToUpper();

            bool valid = true;
            foreach (char c in macStr)
            {
                if ((c > 47 && c < 58) || (c > 64 && c < 71))
                {
                    // 0-1, A-F
                }
                else
                {
                    valid = false;
                    break;
                }
            }

            if (valid && macStr.Length == 12)
                _address = macStr;
        }

        public bool IsValid()
        {
            return _address != _invalid;
        }

        public override string ToString()
        {
            return _address;
        }

        /// <summary>
        /// Returns a formatted HEX string with separators, such as '-',':' or '.'
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        public string ToString(char separator)
        {
            string sep = separator.ToString();
            return _address.Insert(10, sep).Insert(8, sep).Insert(6, sep).Insert(4, sep).Insert(2, sep);
        }

    }
}