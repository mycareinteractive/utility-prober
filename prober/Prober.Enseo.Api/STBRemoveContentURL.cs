﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBRemoveContentURL
    {
        public string RemoveFolder;
        public string RemoveFile;

        public override string ToString()
        {
            return string.Format("RemoveFolder:{1}{0}RemoveFile:{2}",
                Environment.NewLine, RemoveFolder, RemoveFile);
        }
    }
}
