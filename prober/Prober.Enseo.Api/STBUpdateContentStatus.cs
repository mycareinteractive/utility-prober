﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBUpdateContentStatus
    {
        public string Status;
        public string ProgressFile;
        public string ProgressAction;
        public int ProgressValue;
        public int NumSuccess;
        public int NumFailure;
        public int NumTotal;

        public override string ToString()
        {
            return string.Format("Status:{1}{0}ProgressFile:{2}{0}ProgressAction:{3}{0}ProgressValue:{4}{0}NumSuccess:{5}{0}NumFailure:{6}{0}NumTotal:{7}",
                Environment.NewLine, Status, ProgressFile, ProgressAction, ProgressValue, NumSuccess, NumFailure, NumTotal);
        }
    }
}
