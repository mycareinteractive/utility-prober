﻿using System;
using System.Collections.Generic;
using System.Text;
using CookComputing.XmlRpc;
using System.IO;

namespace Prober.Enseo.Api
{
    public interface ISettopBox : IXmlRpcProxy
    {
        [XmlRpcMethod("Enseo.GetVersion")]
        STBVersion GetVersion();

        [XmlRpcMethod("Enseo.GetHardwareId")]
        int GetHardwareId();

        [XmlRpcMethod("Enseo.SetUserData")]
        void SetUserData(string value);

        [XmlRpcMethod("Enseo.GetUserData")]
        string GetUserData();

        [XmlRpcMethod("Enseo.GetHardwareModel")]
        string GetHardwareModel();

        [XmlRpcMethod("Enseo.GetEPLDRevision")]
        string GetEPLDRevision();

        [XmlRpcMethod("Enseo.GetSerialNumber")]
        string GetSerialNumber();

        [XmlRpcMethod("Enseo.SetIREnabled")]
        void SetIREnabled(bool value);
        
        [XmlRpcMethod("Enseo.GetIREnabled")]
        bool GetIREnabled();

        [XmlRpcMethod("Enseo.SetSafeplayEnabled")]
        void SetSafeplayEnabled(bool value);

        [XmlRpcMethod("Enseo.GetSafeplayEnabled")]
        bool GetSafeplayEnabled();

        [XmlRpcMethod("Enseo.SetPowerState")]
        void SetPowerState(string value);

        [XmlRpcMethod("Enseo.GetPowerState")]
        string GetPowerState();

        [XmlRpcMethod("Enseo.Reboot")]
        void Reboot();

        [XmlRpcMethod("Enseo.RestoreFactoryDefaults")]
        void RestoreFactoryDefaults();

        [XmlRpcMethod("Enseo.UpdateFirmware")]
        void UpdateFirmware(STBRemoteURL firmware);

        [XmlRpcMethod("Enseo.UpdateFirmwareStreamPush")]
        void UpdateFirmwareStreamPush(string xmlChannelDescription);

        [XmlRpcMethod("Enseo.GetNetworkDevices")]
        string[] GetNetworkDevices();

        [XmlRpcMethod("Enseo.SetNetworkEnabled")]
        void SetNetworkEnabled(string deviceName, bool value);

        [XmlRpcMethod("Enseo.GetNetworkEnabled")]
        bool GetNetworkEnabled(string deviceName);

        [XmlRpcMethod("Enseo.GetNetworkMAC")]
        string GetNetworkMAC(string deviceName);

        [XmlRpcMethod("Enseo.GetNetworkSettings")]
        STBNetworkSettings GetNetworkSettings(string deviceName);

        [XmlRpcMethod("Enseo.SetNetworkSettings")]
        void SetNetworkSettings(string deviceName, STBNetworkSettings value);

        [XmlRpcMethod("Enseo.GetContentDrives")]
        string[] GetContentDrives();

        [XmlRpcMethod("Enseo.UpdateContent")]
        void UpdateContent(STBUpdateContentURL content);

        [XmlRpcMethod("Enseo.GetUpdateContentStatus")]
        STBUpdateContentStatus GetUpdateContentStatus();

        [XmlRpcMethod("Enseo.RemoveContent")]
        void RemoveContent(STBRemoveContentURL removeURL);

        [XmlRpcMethod("Enseo.GetPlaylistIds")]
        int[] GetPlaylistIds();

        [XmlRpcMethod("Enseo.GetNumOutputs")]
        int GetNumOutputs();

        [XmlRpcMethod("Enseo.SetOutputEnabled")]
        void SetOutputEnabled(int port, bool value);

        [XmlRpcMethod("Enseo.GetOutputEnabled")]
        bool GetOutputEnabled(int port);

        [XmlRpcMethod("Enseo.GetOutputSettings")]
        STBOutputSettings GetOutputSettings();

        [XmlRpcMethod("Enseo.SetOutputSettings")]
        void SetOutputSettings(STBOutputSettings settings);

        [XmlRpcMethod("Enseo.GetOutputEDID")]
        string GetOutputEDID();

        [XmlRpcMethod("Enseo.SetVolume")]
        void SetVolume(int value);

        [XmlRpcMethod("Enseo.GetVolume")]
        int GetVolume();

        [XmlRpcMethod("Enseo.SetMute")]
        void SetMute(bool value);

        [XmlRpcMethod("Enseo.GetMute")]
        bool GetMute();
        // skip 7.39 - 7.42 as they are for legacy STB

        [XmlRpcMethod("Enseo.PlayPlaylist")]
        void PlayPlaylist(int playlistId, int playIndex);

        [XmlRpcMethod("Enseo.PlayChannel")]
        void PlayChannel(string xmlChannelDescription, int playIndex);

        [XmlRpcMethod("Enseo.PlayURL")]
        void PlayURL(string url, int playIndex);

        [XmlRpcMethod("Enseo.Stop")]
        void Stop(int playIndex);

        [XmlRpcMethod("Enseo.Pause")]
        void Pause(int playIndex);

        [XmlRpcMethod("Enseo.Resume")]
        void Resume(int playIndex);

        // Skip 7.49 as it's for legacy

        [XmlRpcMethod("Enseo.GetSettingsXml")]
        string GetSettingsXml();

        [XmlRpcMethod("Enseo.SetSettingsXml")]
        void SetSettingsXml(string settingsXml, bool applyAgainstDefaults=false, string digitalSignature="");

        [XmlRpcMethod("Enseo.ReloadUI")]
        void ReloadUI();

        [XmlRpcMethod("Enseo.SendAuxCommand")]
        string SendAuxCommand(string value);

        [XmlRpcMethod("Enseo.SetSerialPassthrough")]
        void SetSerialPassthrough(STBSerialPassThruSettings settings);

        [XmlRpcMethod("Enseo.SetNetworkDiscoveryEnabled")]
        void SetNetworkDiscoveryEnabled(bool value);

        [XmlRpcMethod("Enseo.GetNetworkDiscoveryEnabled")]
        bool GetNetworkDiscoveryEnabled();

        [XmlRpcMethod("Enseo.AddEventListener")]
        int AddEventListener(string location, int port);

        // The following are undocumented APIs

        [XmlRpcMethod("Enseo.SetLogLevel")]
        void SetLogLevel(string category, int value);

        [XmlRpcMethod("Enseo.GetLogLevel")]
        int GetLogLevel(string category);

        [XmlRpcMethod("Enseo.SetSysLogHost")]
        void SetSysLogHost(string location);

        [XmlRpcMethod("Enseo.DumpSystemLog")]
        bool DumpSystemLog();

        [XmlRpcMethod("Enseo.DumpBrowserLog")]
        bool DumpBrowserLog();
    }

    //public interface ISettopBoxProxy : ISettopBox, IXmlRpcProxy
    //{ }
}
