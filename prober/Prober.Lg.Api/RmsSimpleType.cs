﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Lg.Api
{
    public class RmsSimpleType : IRmsXml
    {
        public string Value { get; private set; }

        public RmsSimpleType()
        {

        }

        public RmsSimpleType(string value)
        {
            Value = value;
        }

        public void ToXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("simpleType");
            writer.WriteString(this.Value);
            writer.WriteEndElement();
        }

        public void FromXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
