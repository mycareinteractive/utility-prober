﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Prober
{
    public partial class STBForm : Form
    {
        private BindingSource bindingSource = new BindingSource();
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();

        public STBForm()
        {
            InitializeComponent();
            Init();
        }

        public String SelectedAddress
        {
            get;
            private set;
        }

        private void Init()
        {
            var dbHost = Properties.Settings.Default.SeaChangeDBHost;
            this.metroTextBoxIP.Text = dbHost;
        }

        private void metroButtonSync_Click(object sender, EventArgs e)
        {
            String connectionString = "Data Source=" + metroTextBoxIP.Text + ",1433;Network Library=DBMSSOCN;"
            + "Initial Catalog=multiverse;User ID=multiverse;Password=multiverse;";

            String selectCommand = "SELECT device_id, hotel_id, room_id, tv_number, device_ip, firmware FROM hm_device";

            try
            {
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // save the IP
                Properties.Settings.Default.SeaChangeDBHost = metroTextBoxIP.Text;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource.DataSource = table;
                dataGridViewSTBs.DataSource = bindingSource;

                dataGridViewSTBs.Columns["device_id"].HeaderText = "Device ID";
                dataGridViewSTBs.Columns["hotel_id"].HeaderText = "Hospital";
                dataGridViewSTBs.Columns["room_id"].HeaderText = "Room & Bed";
                dataGridViewSTBs.Columns["tv_number"].HeaderText = "TV Set";
                dataGridViewSTBs.Columns["device_ip"].HeaderText = "IP Address";
                dataGridViewSTBs.Columns["firmware"].HeaderText = "Firmware";

                // Resize the DataGridView columns to fit the newly loaded content.
                dataGridViewSTBs.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Oops", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewSTBs_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            this.SelectedAddress = dataGridViewSTBs.Rows[e.RowIndex].Cells["device_ip"].Value.ToString();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void metroButtonFilter_Click(object sender, EventArgs e)
        {
            String filterString = "";
            String andString = "";
            if (!StringExtension.IsNullOrWhiteSpace(metroTextBoxRoom.Text))
            {
                filterString += string.Format("room_id LIKE '%{0}%'", metroTextBoxRoom.Text);
                andString = " AND ";
            }

            if (!StringExtension.IsNullOrWhiteSpace(metroTextBoxDevice.Text))
            {
                filterString += andString + string.Format("device_id LIKE '%{0}%'", metroTextBoxDevice.Text);
            }

            this.bindingSource.Filter = filterString;
        }


    }
}
