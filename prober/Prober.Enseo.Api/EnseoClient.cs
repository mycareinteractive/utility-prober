﻿using CookComputing.XmlRpc;
using Prober.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Prober.Enseo.Api
{
    public class EnseoClient
    {
        public delegate void ApiDelegate(ISettopBox proxy);

        private readonly int port = 27275;

        private ILogger logger;
        private XmlRpcLogger xmlLogger;

        public EnseoClient(ILogger log, XmlRpcLogger xmlLog)
        {
            logger = log;
            xmlLogger = xmlLog;
        }

        public bool Reboot(string ip)
        {
            ApiDelegate action = delegate(ISettopBox proxy) { proxy.Reboot();};
            return CallApi(ip, action);
        }

        public bool ReloadUI(string ip)
        {
            ApiDelegate action = delegate(ISettopBox proxy) { proxy.ReloadUI(); };
            return CallApi(ip, action);
        }

        public bool SimulateKey(string ip, NimbusCommand param)
        {
            ApiDelegate action = delegate(ISettopBox proxy) {
                string auxCmd = IRCmd.ToAux(param);
                proxy.SendAuxCommand(auxCmd); 
            };
            return CallApi(ip, action);
        }

        public bool CallApi(string ip, ApiDelegate action)
        {
            bool ret = false;
            var proxy = CreateProxy(ip);
            xmlLogger.SubscribeTo(proxy);
            try
            {
                action(proxy);
                ret = true;
            }
            catch (XmlRpcFaultException faultex)
            {
                logger.Log(faultex.ToString());
                //throw;
            }
            catch (UriFormatException uriex)
            {
                logger.Log(uriex.ToString());
                //throw;
            }
            catch (WebException webex)
            {
                logger.Log(webex.ToString());
                //throw;
            }
            catch (Exception ex)
            {
                logger.Log(ex.ToString());
                throw;
            }
            finally
            {
                xmlLogger.UnsubscribeFrom(proxy);
            }
            return ret;
        }

        private ISettopBox CreateProxy(string ip)
        {
            var url = string.Format(@"http://{0}:{1}/RPC2", ip, port);
            var proxy = XmlRpcProxyGen.Create<ISettopBox>();
            proxy.Url = url;
            proxy.Timeout = 2000;
            proxy.Proxy = null;
            return proxy;
        }

        
    }
}
