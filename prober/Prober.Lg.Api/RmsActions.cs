﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Lg.Api
{
    public sealed class RmsActions
    {
        public static readonly string SystemAction = "system_action";
        public static readonly string LogInitialize = "log_initialize";
        public static readonly string LogSetLevel = "log_set_level";
        public static readonly string LogFinalize = "log_finalize";
    }
}
