﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBVersion
    {
        public int Major;
        public int Minor;
        public int Feature;
        public int Build;

        public override string ToString()
        {
            return string.Format("{0}.{1}.{2}.{3}", Major, Minor, Feature, Build);
        }
    }
}
