﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Lg.Api
{
    public sealed class RmsMethods
    {
        public static readonly string ControlLog = "control_log";
        public static readonly string ControlTv = "control_tv";
        public static readonly string RequestDiagnosis = "request_diagnosis";
    }
}
