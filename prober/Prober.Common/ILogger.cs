﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Common
{
    public interface ILogger
    {
        void Log(String message);
    }
}
