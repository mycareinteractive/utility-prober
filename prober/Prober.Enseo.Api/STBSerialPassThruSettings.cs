﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBSerialPassThruSettings
    {
        public bool Enable;
        public int Baud;
        public int Port;
        public string Device;

        public override string ToString()
        {
            return string.Format("{0} - {1} {2},{3}",
                Device, Enable?"Enabled":"Disabled", Baud, Port);
        }
    }
}
