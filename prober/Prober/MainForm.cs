﻿using System;
using System.Net;
using System.Windows.Forms;
using CookComputing.XmlRpc;
using Prober.Enseo.Api;
using System.Drawing;
using Prober.Common;
using Prober.Lg.Api;

namespace Prober
{
    public partial class MainForm : Form
    {
        public static MainForm theMainForm = null;

        private EnseoClient _enseoApi;
        private LgClient _lgApi;
        private Logger _logger = null;
        private ConsoleForm _console = null;
        private System.Timers.Timer _resultTimer = null;
        private DeviceType _currentDeviceType = DeviceType.None;
        private string _ipAddress = "";

        public MainForm()
        {
            MainForm.theMainForm = this;
            InitializeComponent();
            _logger = new Logger("prober.log");
            _console = new ConsoleForm(_logger);
            _enseoApi = new EnseoClient(_logger, _logger);
            _lgApi = new LgClient(_logger);

            _resultTimer = new System.Timers.Timer(1500);
            _resultTimer.AutoReset = false;
            _resultTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.HideResult);

            string version = AboutBox1.GetVersion();
            Logger.CurrentLogger.Log("==================================================================================");
            Logger.CurrentLogger.Log("Aceso UpCare Prober " + version + " started.");
        }

        ~MainForm()
        {
        }

        public bool Reboot(string ip)
        {
            bool ret = false;
            if (string.IsNullOrEmpty(ip))
            {
                return ret;
            }
            var type = DeviceDetector.Detect(ip);
            if (type == DeviceType.Enseo)
            {
                ret = _enseoApi.Reboot(ip);
            }
            else if (type == DeviceType.Procentric)
            {
                ret = _lgApi.Reboot(ip);
            }
            return ret;
        }

        public bool ReloadUI(string ip)
        {
            bool ret = false;
            if (string.IsNullOrEmpty(ip))
            {
                return ret;
            }
            var type = DeviceDetector.Detect(ip);
            if (type == DeviceType.Enseo)
            {
                ret = _enseoApi.ReloadUI(ip);
            }
            return ret;
        }

        private void ShowApiResult(bool result)
        {
            if (result)
            {
                ShowResult("Command sent.");
            }
            else
            {
                ShowResult("Command error!  Check log file for more details.", true);
            }
        }

        private void ShowResult(String result, bool isError = false)
        {
            metroLabelResult.Text = result;
            metroLabelResult.Left = (this.ClientSize.Width - metroLabelResult.Width) / 2;
            if (isError)
            {
                metroLabelResult.BackColor = Color.Salmon;
            }
            else
            {
                metroLabelResult.BackColor = Color.LemonChiffon;
            }
            metroLabelResult.Visible = true;
            _resultTimer.Stop();
            _resultTimer.Start();
        }

        private void HideResult(object source, System.Timers.ElapsedEventArgs e)
        {
            if (metroLabelResult.InvokeRequired)
            {
                metroLabelResult.Invoke(new MethodInvoker(() => { metroLabelResult.Visible = false; }));
            }
            else
            {
                metroLabelResult.Visible = false;
            }

            _resultTimer.Stop();
        }

        private void EnseoRefreshAction(ISettopBox proxy)
        {
            if (_currentDeviceType != DeviceType.Enseo)
                return;

            try
            {
                this.metroLabelHardware.Text = proxy.GetHardwareModel();
                this.metroLabelFirmware.Text = proxy.GetVersion().ToString();
                this.metroLabelXRev.Text = proxy.GetEPLDRevision();
                this.metroLabelBoard.Text = proxy.GetHardwareId().ToString();
                
                bool mute = proxy.GetMute();
                int volume = proxy.GetVolume();
                this.metroLabelAudio.Text = mute ? "Mute" : "Vol " + volume.ToString();

                string[] devices = proxy.GetNetworkDevices();
                if (devices.Length >= 1)
                {
                    DeviceMacAddress mac = new DeviceMacAddress(proxy.GetNetworkMAC(devices[0]));
                    this.metroLabelNet0.Text = devices[0] + " :";
                    this.metroLabelMac0.Text = mac.ToString(':');
                }
                if (devices.Length >= 2)
                {
                    DeviceMacAddress mac = new DeviceMacAddress(proxy.GetNetworkMAC(devices[1]));
                    this.metroLabelNet1.Text = devices[1] + " :";
                    this.metroLabelMac1.Text = mac.ToString(':');
                }

                int appLevel = proxy.GetLogLevel("App");
                int libLevel = proxy.GetLogLevel("Lib");
                int nimbusLevel = proxy.GetLogLevel("App");
                this.metroLabelLogLevels.Text = string.Format("App {0}  Lib {1}  Nimbus {2}", appLevel, libLevel, nimbusLevel);

                this.metroLabelPower.Text = proxy.GetPowerState();

                STBOutputSettings vout = proxy.GetOutputSettings();
                this.metroLabelVideo.Text = vout.Connector + " " + vout.TVStandard;
            }
            catch(Exception ex)
            {
                _logger.Log("Error during refreshing: " + ex.Message);
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            metroTextBoxIP.Focus();
        }

        
        private void imageButtonSearch_Click(object sender, EventArgs e)
        {
            var form = new STBForm();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.metroTextBoxIP.Text = form.SelectedAddress;
                _currentDeviceType = DeviceType.None;
                _currentDeviceType = DeviceDetector.Detect(metroTextBoxIP.Text);
            }
        }

        private void imageButtonRefresh_Click(object sender, EventArgs e)
        {
            if (_currentDeviceType == DeviceType.Enseo)
            {
                _enseoApi.CallApi(metroTextBoxIP.Text, EnseoRefreshAction);
            }
        }

        private void metroTextBoxIP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                imageButtonRefresh.Focus();
                imageButtonRefresh.PerformClick();
            }
        }

        private void metroButtonConsole_Click(object sender, EventArgs e)
        {
            if (_currentDeviceType == DeviceType.None)
                _currentDeviceType = DeviceDetector.Detect(metroTextBoxIP.Text);

            if (_console.Visible)
            {
                if (_console.WindowState == FormWindowState.Minimized)
                {
                    _console.WindowState = FormWindowState.Normal;
                    return;
                }
            }
            else
            {
                _console.Show();
                _console.WindowState = FormWindowState.Normal;
                _console.Start(this._ipAddress, _currentDeviceType);
            }
        }

        #region Regular remote buttons

        private void imageButtonPower_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.OnOff));
        }

        private void imageButtonUp_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Up));
        }

        private void imageButtonDown_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Down));
        }

        private void imageButtonLeft_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Left));
        }

        private void imageButtonRight_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Right));
        }

        private void imageButtonSelect_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Select));
        }

        private void imageButtonPlay_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.PlayPause));
        }

        private void imageButtonStop_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Stop));
        }

        private void imageButtonCC_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.CCAlt));
        }

        private void imageButtonMenu_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Menu));
        }

        private void imageButtonPrev_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.PrevChan));
        }

        private void imageButtonMute_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Mute));
        }

        private void imageButtonChUp_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.ChanPlus));
        }

        private void imageButtonChDown_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.ChanMinus));
        }

        private void imageButtonVolUp_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.VolPlus));
        }

        private void imageButtonVolDown_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.VolMinus));
        }

        private void imageButton1_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key1));
        }

        private void imageButton2_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key2));
        }

        private void imageButton3_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key3));
        }

        private void imageButton4_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key4));
        }

        private void imageButton5_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key5));
        }

        private void imageButton6_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key6));
        }

        private void imageButton7_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key7));
        }

        private void imageButton8_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key8));
        }

        private void imageButton9_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key9));
        }

        private void imageButton0_Click(object sender, EventArgs e)
        {
            ShowApiResult(_enseoApi.SimulateKey(_ipAddress, NimbusCommand.Key0));
        }

        #endregion

        private void metroButtonReload_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (_currentDeviceType == DeviceType.Enseo)
            {
                result = _enseoApi.ReloadUI(_ipAddress);
            }
            ShowApiResult(result);
        }

        private void metroButtonReboot_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (_currentDeviceType == DeviceType.Enseo)
            {
                result = _enseoApi.Reboot(_ipAddress);
            }
            else if(_currentDeviceType == DeviceType.Procentric)
            {
                result = _lgApi.Reboot(_ipAddress);
            }
            ShowApiResult(result);
        }

        private void metroTextBoxIP_TextChanged(object sender, EventArgs e)
        {
            _ipAddress = metroTextBoxIP.Text;
            _currentDeviceType = DeviceType.None;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new AboutBox1();
            dialog.ShowDialog(this);
        }

        private void metroTextBoxIP_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _ipAddress = metroTextBoxIP.Text;
            _currentDeviceType = DeviceDetector.Detect(_ipAddress);
        }

        private void openLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = _logger.GetFilePath();
            System.Diagnostics.Process.Start(path);
        }




    }
}
