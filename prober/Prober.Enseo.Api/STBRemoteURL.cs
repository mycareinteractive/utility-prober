﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public struct STBRemoteURL
    {
        public string Location;
        public int Port;
        public string TransferProtocol;
        public string Username;
        public string Password;
        public string FilePath;

        public override string ToString()
        {
            return string.Format("{0}://{1}:{2}@{3}:{4}/{5}",
                TransferProtocol, Username, Password, Location, Port, FilePath);
        }
    }
}
