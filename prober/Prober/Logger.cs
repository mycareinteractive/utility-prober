﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Prober.Enseo.Api;
using CookComputing.XmlRpc;
using Prober.Common;

namespace Prober
{
    class Logger : XmlRpcLogger, ILogger
    {
        public static Logger CurrentLogger = null;
        private StreamWriter logFile = null;
        private string filePath;

        public Logger(string filename, int sizeInMb = 50)
        {
            if (Logger.CurrentLogger == null)
            {
                // first log is default log
                Logger.CurrentLogger = this;
            }
            filePath = Application.StartupPath + "\\" + filename;
            this.TrimFile(filePath, sizeInMb * 1024, 0.2);
            logFile = new System.IO.StreamWriter(filePath, true);
        }

        ~Logger()
        {
        }

        public void Log(String message)
        {
            String str = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff ");
            str += message;
            logFile.WriteLine(str);
            logFile.Flush();
        }

        public void LogRaw(string message)
        {
            logFile.Write(message);
            logFile.Flush();
        }

        public string GetFilePath()
        {
            return filePath;
        }

        protected override void OnRequest(object sender, XmlRpcRequestEventArgs e)
        {
            DumpStream(e.RequestStream);
        }

        protected override void OnResponse(object sender, XmlRpcResponseEventArgs e)
        {
            DumpStream(e.ResponseStream, true);
        }

        private void DumpStream(Stream stm, bool isResponse = false)
        {
            String message = "[XMLRPC] ";
            if (isResponse)
            {
                message += "<<<<<<<<<< Response <<<<<<<<<<" + Environment.NewLine;
            }
            else
            {
                message += ">>>>>>>>>> Request >>>>>>>>>>" + Environment.NewLine;
            }
            
            stm.Position = 0;
            TextReader trdr = new StreamReader(stm);
            message += trdr.ReadToEnd();

            this.Log(message);
        }

        private void TrimFile(string filename, decimal cap, double rollingRatio)
        {
            if (System.IO.File.Exists(filename))
            {
                var FileSize = Convert.ToDecimal((new System.IO.FileInfo(filename)).Length);

                if (FileSize > cap)
                {
                    var file = new List<String>(File.ReadAllLines(filename));
                    var AmountToCull = (int)(file.Count * rollingRatio);
                    file.RemoveRange(0, AmountToCull);
                    var trimmed = file.ToArray();
                    File.WriteAllLines(filename, trimmed);
                }
            }
        }
    }
}
