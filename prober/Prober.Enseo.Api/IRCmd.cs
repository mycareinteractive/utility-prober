﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prober.Enseo.Api
{
    public static class IRCmd
    {
        public static string ToName(NimbusCommand cmd)
        {
            return string.Format("{0:G}", cmd);
        }

        public static string ToInt(NimbusCommand cmd)
        {
            return string.Format("{0:D}", cmd);
        }

        public static string ToHex(NimbusCommand cmd)
        {
            return string.Format("0x{0:X4}", (int)cmd);
        }

        /// <summary>
        /// You can use ToAux() to simulate IR key strokes via NetworkAPI.
        /// </summary>
        /// <param name="cmd">One of the key command in enum NimbusCommand</param>
        /// <returns></returns>
        /// <example>
        /// ISettopBox proxy = Factory.Create("http://192.168.100.21:27275/PRC2");
        /// string result = proxy.SendAuxCommand(IRCmd.ToAux(NimbusCommand.Menu));
        /// </example>
        public static string ToAux(NimbusCommand cmd)
        {
            return string.Format("CKEY 0x{0:X4}", (int)cmd);
        }

        public static string ToAux(int cmdInt)
        {
            return string.Format("CKEY 0x{0:X4}", cmdInt);
        }

        public static string ToAux(string cmdHexString)
        {
            return string.Format("CKEY {0}", cmdHexString);
        }
    }

    public enum NimbusCommand
    {
        Base = 0xf000,
        OnOff = 0xf001,	                // On/off toggle
        On = 0xf043,	                // On
        Off = 0xf044,	                // Off
        OnAlt = 0xf036,	                // On
        OffAlt = 0xf037,	            // Off

        Up = 0xf002,	                // Nav up
        Down = 0xf003,	                // Nav down
        Left = 0xf004, 	                // Nav left
        Right = 0xf005,     	        // Nav right
        Select = 0xf006,	            // Nav select

        Menu = 0xf007,	                // Menu
        Guide = 0xf096,	                // Guide
        Back = 0xf097,	                // Back
        Cancel = 0xf098,	            // Cancel

        VolPlus = 0xf008,	            // Volume plus
        VolMinus = 0xf009,	            // Volume minus
        Mute = 0xf00b,	                // Mute toggle
        MuteOn = 0xf00c,	            // Mute On
        MuteOff = 0xf00d,	            // Mute Off

        Key1 = 0xf00e,	                // Digit keys
        Key2 = 0xf00f,
        Key3 = 0xf010,
        Key4 = 0xf011,
        Key5 = 0xf012,
        Key6 = 0xf013,
        Key7 = 0xf014,
        Key8 = 0xf015,
        Key9 = 0xf016,
        Key0 = 0xf017,

        Play = 0xf018,	                // Play
        PlayPause = 0xf018,	            // Play/pause
        Stop = 0xf019,	                // Stop
        Pause = 0xf01a,	                // Pause
        FastRewind = 0xf01b,	        // Rewind
        FastForward = 0xf01c,	        // Fast forward
        SlowRewind = 0xf01d,	        // Slow Rewind
        SlowForward = 0xf01e,	        // Slow forward
        PrevTrack = 0xf01f,	            // Previous
        NextTrack = 0xf020, 	        // Next

        KeyF = 0xf029,                  // Key F

        ChanPlus = 0xf02b,	            // Channel plus
        ChanMinus = 0xf02c,	            // Channel minus
        PrevChan = 0xf02d,	            // Previous channel viewed

        Sleep = 0xf02e,	                // Sleep mode selection
        SleepAlt = 0xf030,	            // Sleep mode selection
        ScreenFormatAlt = 0xf035,    	// Screen format selection
        CCAlt = 0xf055,	                // CC control

        SetupKeySequence = 0xf04d,      // Setup Key Sequence

        SAP = 0xf04e,	                // Secondary audio program

        DisplayLabel = 0xf04c,	        // Display channel label
        Status = 0xf02a,	            // Display status screen

        SelectComposite = 0xf073,	    // Select composite input
        SelectVGA = 0xf076,	            // Select VGA input
        SelectHDMI = 0xf077,	        // Select HDMI input

        TuneDefault = 0xf078,	        // Select the default channel/input

        // The following Select* commands expect Cmd.Params
        // to contain a number specifying the input instance.
        SelectTuner = 0xf066,	        // Select Enseo Tuner
        SelectCompositeAlt = 0xf067,	// Select Composite Input (TV or Enseo)
        SelectSVideo = 0xf068,	        // Select S-Video Input (TV or Enseo)
        SelectComponent = 0xf069,	    // Select Component Input
        SelectVGAAlt = 0xf06a,	        // Select VGA Input
        SelectHDMIAlt = 0xf06b,	        // Select DVI/HDMI Input

        AppSpecificA = 0xf056,	        // Special function keys with application-specific purposes
        AppSpecificB = 0xf057,	        //   such as for use by Javascript-based VOD applications
        AppSpecificC = 0xf058,
        AppSpecificD = 0xf059,
        AppSpecificE = 0xf05a,
        AppSpecificF = 0xf05b,
        AppSpecificG = 0xf05c,
        AppSpecificH = 0xf05d,
        AppSpecificI = 0xf05e,
        AppSpecificJ = 0xf05f,
        AppSpecificK = 0xf0c0,
        AppSpecificL = 0xf0c1,
        AppSpecificM = 0xf0c2,
        AppSpecificN = 0xf0c3,
        AppSpecificO = 0xf0c4
    }
}
